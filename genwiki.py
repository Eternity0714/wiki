# -*- coding: utf-8
import os
import sys
import markdown

filter = ['.git']

def listdir(root):
    l = []
    for name in os.listdir(root):
        if not (name in filter):
            path = os.path.join(root, name)
            if os.path.isfile(path):
                l.append(path)
            else:
                l.extend(listdir(path))
    return l

def md2html(mdstr):
    exts = ['markdown.extensions.extra', 'markdown.extensions.codehilite','markdown.extensions.tables','markdown.extensions.toc']

    demofile = open("./demo.html", 'r')
    demo = demofile.read()
    demofile.close()

    ret = markdown.markdown(mdstr,extensions=exts)
    ret = demo.format(ret)
    ret = ret.replace(".md", ".html")
    return ret

def mdfile2htmlfile(inpath, outpath):
    infile = open(inpath, 'r')
    md = infile.read()
    infile.close()

    if os.path.exists(outpath):
        os.remove(outpath)

    dirname = os.path.dirname(outpath)
    if not os.path.exists(dirname):
        os.makedirs(dirname)

    outfile = open(outpath, 'w')
    outfile.write(md2html(md))
    outfile.close()


if __name__ == '__main__':
    root = os.path.join(sys.argv[1], "")
    if not os.path.exists(root):
        exit(0)

    target = os.path.join(sys.argv[2], "")

    for path in listdir(root):
        split = os.path.splitext(path)
        if split[-1] == ".md":
            outpath = split[0].replace(root, target) + ".html"
            mdfile2htmlfile(path, outpath)