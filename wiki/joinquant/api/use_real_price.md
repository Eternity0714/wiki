## use_real_price 设置动态复权(真实价格)模式

``` python
set_option('use_real_price', value)
```

> * 该设定必须在`initialize`中使用