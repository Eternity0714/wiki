## set_order_cost 设置佣金/印花税

> 指定每笔交易需要收取的手续费，系统会根据用户指定的费率计算每笔交易的手续费

<br>

### 参数
| 参数 | 类型 | 描述 |
| :----: | :----: | ---- |
| cost | [OrderCost][OrderCost] | 手续费 |
| type | string | [交易品种][交易品种] |
| ref | string | 参考代码，支持股票代码/基金代码/期货合约代码，以及期货的品种 |

> 注意：针对特定的交易品种类别设置手续费时，必须将`ref`设置为`None`; 若针对特定的交易品种或者标的，需要将`type`设置为对应的交易品种类别，将`ref`设置为对应的交易品种或者标的

<br>

### 示例
``` python
cost = OrderCost(open_tax=0, close_tax=0.001, open_commission=0.0003, close_commission=0.0003, close_today_commission=0, min_commission=5)

set_order_cost(cost, type = 'stock')
```

<br>

> 注: 期货持仓到交割日会以当天结算价平仓，没有手续费，不会有交易记录



[OrderCost]:./OrderCost.md
[交易品种]:./交易品种.md