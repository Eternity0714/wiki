## set_slippage 设置滑点

> * 设置滑点，回测/模拟时有效
> * 当你下单后，真实的成交价格与下单时的预期价格总会有一定偏差，因此加入滑点模式来帮助你更好的模拟真实市场的表现。暂时只支持[固定滑点][固定滑点]。同时也支持为交易品种和特定的交易标的设置滑点

<br>

### 参数
| 参数 | 类型 | 描述 |
| :----: | :----: | ---- |
| slippage | [Slippage][固定滑点] | 固定滑点值 |
| type | string | [交易品种][交易品种], 为`None`时则应用于全局。当`type`被设定而`ref`为`None`时，表是将滑点应用于交易品种为`type`的所有交易标的 |
| ref | string | 标的代码。如果要为特定交易标的单独设置滑点，必须同时设置`type`为交易标的的交易品种 |


### 示例
``` python
# 为全部交易品种设定固定值滑点
set_slippage(FixedSlippage(0.02))

# 为股票设定滑点为百分比滑点
set_slippage(PriceRelatedSlippage(0.00246),type='stock')

# 设置CU品种的滑点为跳数滑点2
set_slippage(StepRelatedSlippage(2),type='futures',ref = 'CU') 

# 为螺纹钢RB1809设定滑点为跳数滑点(注意只是这一个合约，不是所有的RB合约)
set_slippage(StepRelatedSlippage(2),type='futures', ref="RB1809.XSGE")
# StepRelatedSlippage(2)表示开平的单边滑点为1个价格最小单位，螺纹钢价格最小变动单位为1元/吨
# 如果以市价单进行开多仓（或者平空仓），现价3000元，成交价3000+1*2/2=3001元
# 如果以市价单进行平空仓（或者平多仓），现价3000元，成交价3000-1*2/2=2999元
```

> 注：
> 1. 如果你没有调用`set_slippage`函数，系统默认的滑点是`PriceRelatedSlippage(0.00246)`
> 2. 所有类型为`mmf`与`money_market_fund`的标的滑点默认为0，且调用`set_slippage`重新设置也不会生效


[固定滑点]:./固定滑点.md
[交易品种]:./交易品种.md