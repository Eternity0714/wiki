## OrderCost 交易成本类

> 用于记录每笔交易的手续费和印花税

| 参数 | 类型 | 描述 |
| :----: | :----: | ---- |
| open_tax | number | 买入时印花税(只股票类标的收取，基金与期货不收) |
| close_tax | number | 卖出时印花税(只股票类标的收取，基金与期货不收) |
| open_commission | number | 买入时佣金，申购场外基金的手续费 |
| close_commission | number | 卖出时佣金，赎回场外基金的手续费 |
| close_today_commission | number | 平今仓佣金 |
| min_commission | number | 最低佣金，不包含印花税 |

<br>

### 示例
``` python
OrderCost(open_tax=0, close_tax=0.001, open_commission=0.0003, close_commission=0.0003, close_today_commission=0, min_commission=5)
```