#markdown基础语法

###目录
1. [段落和换行](#1)
2. [标题](#2)
3. [区块引用](#3)
4. [列表](#4)
5. [代码区块](#5)
6. [分隔线](#6)
7. [链接和图片](#7)
8. [参考式链接](#8)
9. [自动邮箱链接](#9)
10. [粗体、斜体和删除线](#10)
11. [开头空格](#11)
12. [行内空格](#12)
13. [反斜杠](#13)
14. [代码块](#14)
15. [内容目录](#15)
16. [脚注](#16)
17. [标签和分类](#17)
18. [待办事宜 Todo 列表](#18)
19. [表格](#19)


#### <span id="1">1. 段落和换行</span>
一个 Markdown 段落是由一个或多个连续的文本行组成，它的前后要有一个以上的空行。

#### <span id="2">2. 标题</span>
用#标识符表示，例如：

![标题](./images/标题.png)

#### <span id="3">3. 区块引用</span>
* 在段落的第一行最前面加">"

![区块引用](./images/区块引用01.png)

* 区块引用可以嵌套（例如：引用内的引用），只要根据层次加上不同数量的 > ：

![区块引用](./images/区块引用02.png)

* 区块内也可以套用其他的 Markdown 语法，包括加粗、列表、代码区块等：

![区块引用](./images/区块引用03.png)

#### <span id="4">4. 列表</span>
Markdown 支持有序列表和无序列表。

* 无序列表使用星号、加号或是减号作为列表标记，效果一样：

![无序列表](./images/无序列表.png)

* 有序列表则使用数字接着一个英文句点：

![有序列表](./images/有序列表.png)

#### <span id="5">5. 代码区块</span>
要在 Markdown 中建立代码区块很简单，只要简单地缩进 4 个空格或是 1 个制表符就可以，例如，下面的输入：

![代码区块](./images/代码区块.png)

#### <span id="6">6. 分隔线</span>
你可以在一行中用三个以上的星号、减号、底线来建立一个分隔线，行内不能有其他东西。你也可以在星号或是减号中间插入空格。下面每种写法都可以建立分隔线：

![分隔线](./images/分隔线01.png)

显示的效果都一样：

![分隔线](./images/分隔线02.png)

#### <span id="7">7. 链接和图片</span>

链接：在 Markdown 中，插入链接只需要使用 `[显示文本](链接地址)` 即可。

图片：在 Markdown 中，插入图片只需要使用 `![显示文本](图片链接地址)` 即可。

图片链接：在 Markdown 中，插入图片只需要使用 `[![显示文本](图片链接地址)](链接地址)` 即可。

_注：插入图片的语法和链接的语法很像，只是前面多了一个 ！_

| 语法 | 例子 | 效果 |
| --- | --- | ---|
| `[显示文本](链接地址)` | \[百度](https://www.baidu.com) | [百度](https://www.baidu.com) |
| `![显示文本](图片链接地址)` | \!\[百度](https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo_top_ca79a146.png) | ![百度](https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo_top_ca79a146.png) |
| `[![显示文本](图片链接地址)](链接地址)` | \[\!\[百度](https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo_top_ca79a146.png)](https://www.baidu.com) | [![百度](https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo_top_ca79a146.png)](https://www.baidu.com) |

#### <span id="8">8. 参考式链接</span>
Here is [Baidu] and [Sina][1] link.
[Baidu]: https://www.baidu.com "Baidu"
[1]: http://www.sina.com "Sina"
![参考式链接](./images/参考式链接.png)

#### <span id="9">9. 自动邮箱链接</span>
Markdown支持以比较简短的自动链接形式来处理电子邮件信箱，例如：<372929104@qq.com>

![自动邮箱链接](./images/自动邮箱链接.png)

#### <span id="10">10. 粗体、斜体和删除线</span>
Markdown 的粗体和斜体也非常简单：

用`两个 * 包含一段文本`就是粗体的语法；

用`一个 * 包含一段文本`就是斜体的语法。

用`两个 ~ 包含一段文本`就是删除线的语法。

![粗体和斜体](./images/粗体斜体.png)

#### <span id="11">11. 开头空格</span>
Markdown语法会忽略首行开头的空格，如果要体现出首行开头空两个的效果，可以使用 全角符号下的空格 ，windows下使用 shift+空格 切换。

![开头空格](./images/开头空格.png)

#### <span id="12">12. 行内空格</span>
行内标记用反引号把它包起来\` `，例如：

![行内标记](./images/行内标记.png)

#### <span id="13">13. 反斜杠</span>
Markdown可以利用反斜杠来插入一些在语法中有其它意义的符号，例如：如果你想要用星号加在文字旁边的方式来做出强调效果，你可以在星号的前面加上反斜杠：

![反斜杠](./images/反斜杠.png)

#### <span id="14">14. 代码块</span>
使用“```”+“语言名称”进行标记。例如：

* python实例：
![代码块-python](./images/代码块-python.png)

* JavaScript示例：
![代码块-javascript](./images/代码块-javascript.png)

#### <span id="15">15. 内容目录</span>
使用[TOC]引用目录

![内容目录](./images/内容目录.png)

**备注：**有一些编辑器不支持[TOC]标记，比如码云不支持。

#### <span id="16">16. 脚注</span>
使用 [^keyword] 表示注脚。

![脚注](./images/脚注.png)

**备注：**关于注脚好像每个编辑器表示方式会有所不用，以上截图来自“作业部落”。

#### <span id="17">17. 标签和分类</span>
一般在文首输入tags添加标签，categories添加分类：

    tags: 
    - Markdown
    - 语言
    categories:
    - 技术

**备注：**分类具有层次性，标签无层次性。

#### <span id="18">18. 待办事宜 Todo 列表</span>

待办事项和清单在日常工作、生活中经常被使用。

在Markdown中，你只需要在待办的事项文本或者清单文本前加上- [ ]、- [x]即可。

\- \[ ] 表示未完成，- [x] 表示已完成。

注：键入字符与字符之间都要保留一个字符的空格。

具体呈现如下：

![todo](./images/todo.png)


#### <span id="19">19. 表格</span>
![表格](./images/表格.png)